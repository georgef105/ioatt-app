import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { ServiceWorkerService } from '../service-worker/service-worker.service';
import { NotificationService } from '../notification/notification.service';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class InitGuard implements CanActivate {
  constructor(
    private serviceWorkerService: ServiceWorkerService,
    private notificationService: NotificationService,
    private userService: UserService
  ) {}

  canActivate(): boolean {
    this.userService.init();
    this.serviceWorkerService.init();
    this.notificationService.init();

    return true;
  }
}
