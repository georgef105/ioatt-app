import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';

import { environment } from 'src/environments/environment';
import { messaging } from 'firebase/app';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceWorkerService {
  public ready = new BehaviorSubject(false);

  constructor(
    private swUpdates: SwUpdate
  ) { }

  init() {
    this.swUpdates.available.subscribe(_ => this.swUpdates.activateUpdate().then(() => {
      console.log('reloading for update');
      document.location.reload();
    }));

    navigator.serviceWorker.ready.then(swr => {
      console.log('swr', swr);
      messaging().useServiceWorker(swr);
      messaging().usePublicVapidKey(environment.vapidPublicKey);
    }).then(() => this.ready.next(true));
  }

  public unregister(): void {
    navigator.serviceWorker.ready.then(swr => swr.unregister());
  }
}
