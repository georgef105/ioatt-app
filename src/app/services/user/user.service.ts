import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

import { auth, User as FbUser } from 'firebase/app';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

export interface User {
  displayName: string | null;
  email: string | null;
  photoURL: string | null;
  uid: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private user: Observable<User>;

  constructor(
    private afAuth: AngularFireAuth
  ) { }

  public init(): void {
    this.user = this.afAuth.authState.pipe(
      map(fbUser => this.mapToUser(fbUser)),
      shareReplay()
    );
  }

  public login(): void {
    console.log('logging in');
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
  }

  public logout(): void {
    this.afAuth.auth.signOut();
  }

  public getUser(): Observable<User> {
    return this.user;
  }

  private mapToUser(fbUser: FbUser): User {
    return fbUser && {
      displayName: fbUser.displayName,
      email: fbUser.email,
      photoURL: fbUser.photoURL,
      uid: fbUser.uid
    };
  }
}
