import { Injectable } from '@angular/core';
import { SwPush } from '@angular/service-worker';
import { messaging } from 'firebase/app';
import { AngularFireDatabase } from '@angular/fire/database';
import { UserService } from '../user/user.service';
import { take, delay, filter, tap } from 'rxjs/operators';
import { ServiceWorkerService } from '../service-worker/service-worker.service';
import { combineLatest } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(
    private swPush: SwPush,
    private afDB: AngularFireDatabase,
    private userService: UserService,
    private serviceWorkerService: ServiceWorkerService,
    private snackBar: MatSnackBar
  ) { }

  public init() {
    this.swPush.messages.subscribe(message => this.handlePushNotification(message));

    messaging().onTokenRefresh(token => this.updateUserToken(token));

    combineLatest(this.userService.getUser(), this.serviceWorkerService.ready).pipe(
      filter(([user]) => !!user),
      take(1),
      delay(500)
    ).subscribe(() => this.requestPermission());
  }

  public requestPermission(): void {
    Notification.requestPermission().then(() => messaging().getToken())
      .then(token => this.updateUserToken(token))
      .catch(err => console.error('error getting token', err));
  }

  private handlePushNotification(message: object): void {
    console.log('message', message);
    this.snackBar.open('message', 'close', { duration: 5000 });
  }

  private updateUserToken(registrationToken: string): void {
    console.log('got token', registrationToken);

    this.userService.getUser().pipe(
      take(1)
    ).subscribe(user => {
      if (!user) {
        throw new Error('User data not found');
      }
      this.afDB.object(`users/${user.uid}`).set({ registrationToken });
    });
  }
}
