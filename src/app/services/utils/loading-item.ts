import { OperatorFunction } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

export interface LoadingItem<T> {
  item: T;
  isLoading: boolean;
  error?: Error;
}

export function wrapInLoadingState<T>(): OperatorFunction<T, LoadingItem<T>> {
  return source => {
    return source.pipe(
      map(item => {
        return {
          item,
          isLoading: false
        };
      }),
      startWith({
        item: null,
        isLoading: true
      })
    );
  };
}