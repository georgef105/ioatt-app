import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShellComponent } from './shell.component';
import { InitGuard } from '../services/init/init.guard';

const routes: Routes = [
  {
    path: '',
    component: ShellComponent,
    canActivate: [InitGuard],
    children: [
      {
        path: '',
        loadChildren: () => import('../home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'devices',
        loadChildren: () => import('../devices/devices.module').then(m => m.DevicesModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShellRoutingModule { }
