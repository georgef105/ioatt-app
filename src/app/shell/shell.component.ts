import { Component, OnInit } from '@angular/core';
import { UserService, User } from '../services/user/user.service';
import { Observable } from 'rxjs';
import { LoadingItem, wrapInLoadingState } from '../services/utils/loading-item';
import { tap } from 'rxjs/operators';
import { ServiceWorkerService } from '../services/service-worker/service-worker.service';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})
export class ShellComponent implements OnInit {
  public user$: Observable<LoadingItem<User>>;

  constructor(
    private userService: UserService,
    private serviceWorkerService: ServiceWorkerService
  ) { }

  ngOnInit() {
    this.user$ = this.userService.getUser().pipe(
      wrapInLoadingState(),
      tap(user => console.log('user', user))
    );
  }

  public login(): void {
    this.userService.login();
  }

  public logout(): void {
    this.userService.logout();
  }

  public unregisterSW(): void {
    this.serviceWorkerService.unregister();
  }
}
