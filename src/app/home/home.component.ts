import { Component, OnInit } from '@angular/core';

import { mergeMapTo } from 'rxjs/operators';
import { SwPush, SwUpdate } from '@angular/service-worker';
import { environment } from 'src/environments/environment';

import * as firebase from 'firebase/app';
import 'firebase/messaging';
import { NotificationService } from '../services/notification/notification.service';
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

}
