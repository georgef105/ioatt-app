import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { combineLatest } from 'rxjs';

import { UserService } from '../services/user/user.service';

export interface Device {
  key: string;
  displayName: string;
  lastCommunication: number;
  lastMotion: number;
  usersToNotify: Record<string, boolean>;
  wifi: string;
  owner: string;


  // computed values
  isSubscribed: boolean;
  isActive: boolean;
}

const INACTIVE_CUTOFF_TIME = 5 * 1000 * 60; // 5 mins

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  constructor(
    private afDB: AngularFireDatabase,
    private userService: UserService
  ) { }

  public getDevices(): Observable<Device[]> {
    const devices$ = this.afDB.object<Record<string, Device>>('devices').valueChanges().pipe(
      map(deviceRecord => Object.keys(deviceRecord).map(key => ({ key, ...deviceRecord[key] })))
    );

    return combineLatest(devices$, this.userService.getUser()).pipe(
      map(([devices, user]) => {
        return devices.map(device => {
          return {
            ...device,
            isSubscribed: device.usersToNotify && device.usersToNotify[user.uid],
            isActive: Date.now() - device.lastCommunication < INACTIVE_CUTOFF_TIME
          };
        });
      })
    );
  }

  public subscribeToDeviceNotifications(deviceKey: string): void {
    this.userService.getUser().pipe(
      take(1)
    ).subscribe(user => {
      this.afDB.object(`devices/${deviceKey}/usersToNotify`).update({ [user.uid]: true });
    });
  }

  public unsubscribeToDeviceNotifications(deviceKey: string): void {
    this.userService.getUser().pipe(
      take(1)
    ).subscribe(user => {
      this.afDB.object(`devices/${deviceKey}/usersToNotify/${user.uid}`).remove();
    });
  }
}
