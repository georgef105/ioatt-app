import { Component, Input } from '@angular/core';
import { Device, DeviceService } from '../device.service';

@Component({
  selector: 'app-device-card',
  templateUrl: './device-card.component.html',
  styleUrls: ['./device-card.component.scss']
})
export class DeviceCardComponent {
  @Input() public device: Device;

  constructor(
    private deviceService: DeviceService
  ) { }

  public subscribeToNotification(): void {
    this.deviceService.subscribeToDeviceNotifications(this.device.key);
  }

  public unsubscribeToNotification(): void {
    this.deviceService.unsubscribeToDeviceNotifications(this.device.key);
  }
}
