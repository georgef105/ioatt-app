// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDKCGbQDLw6t0olPwMaOJTpvS6dhuRLin8',
    authDomain: 'ioatt-236900.firebaseapp.com',
    databaseURL: 'https://ioatt-236900.firebaseio.com',
    projectId: 'ioatt-236900',
    storageBucket: 'ioatt-236900.appspot.com',
    messagingSenderId: '267523137897',
    appId: '1:267523137897:web:89ded03c8fcdd0b2735df8'
  },
  vapidPublicKey: 'BA-u8rfCzsar3Eox9W_KUcqsVZ7_DIozUrIGqeSTrGpBllxp5jCc-OXv2pTMdqfeK0bKHel8ZUnRHnWVjGDNFhQ'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
