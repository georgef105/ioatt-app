import * as admin from 'firebase-admin';
import { Message } from 'firebase-functions/lib/providers/pubsub';
import { EventContext, pubsub } from 'firebase-functions';

admin.initializeApp();

interface User {
  registrationToken: string;
  name: string;
}

export const iotDeviceUpdate = pubsub.topic('env-data-pub').onPublish(async (message: Message, context: EventContext) => {
  console.log('got message', message.json);

  const deviceId = message.attributes.deviceId;

  const update = getDeviceUpdate(message.json);

  await admin.database().ref('/devices').child(deviceId).update(update);

  const shouldSendNotification = message.json.type === 'STATE_CHANGE'
    && message.json.change === 'MOTION_DETECTED';

  if (shouldSendNotification) {
    await notifyUsers(deviceId, context.timestamp);
  }
  
  return Promise.resolve();
});

function getUsersToNotify (deviceId: string): Promise<Array<User>> {
  return admin.database().ref('/devices').child(deviceId).child('usersToNotify').once('value')
    .then(snapshot => snapshot.val())
    .then((userRecord: Record<string, boolean>) => Object.keys(userRecord))
    .then((userKeys: Array<string>) => Promise.all((userKeys || []).map(userKey => 
      admin.database().ref('/users').child(userKey).once('value'))))
    .then(userRefs => userRefs.map(userRef => userRef.val()))
    .then(users => users.filter(user => !!user));
}

async function notifyUsers (deviceId: string, timestamp: string): Promise<void> {

  const usersToNotify = await getUsersToNotify(deviceId);

  const fmcMessage: admin.messaging.MessagingPayload = {
    data: {
      when: timestamp
    },
    notification: {
      title: 'Motion Sensed',
      body: `motion sensed on ${deviceId}`
    }
  };

  await Promise.all(usersToNotify.map(user => admin.messaging().sendToDevice(user.registrationToken, fmcMessage)));
}

function getDeviceUpdate (messageData: any): object {
  const lastCommunication = Date.now();

  const baseUpdate = {
    lastCommunication
  };

  if (messageData.type === 'STATUS_UPDATE') {
    return {
      ...baseUpdate,
      wifi: messageData.wifiStrength
    };
  }

  if  (messageData.type === 'STATE_CHANGE' && messageData.change === 'MOTION_DETECTED') {
    return {
      ...baseUpdate,
      lastMotion: lastCommunication
    };
  }

  return baseUpdate;
}